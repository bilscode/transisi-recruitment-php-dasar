<?php

function nilaiUjian($nilai)
{
    $value = array_map('intval', explode(' ', $nilai));
    
    print 'Nilai awal => '; 
    print implode(', ', $value);
    print '<br/>';

    //Rata - rata
    print 'Rata-rata => '. round(array_sum($value)/count($value), 2); 
    print '<br/>';
    
    //Sort tertinggi
    rsort($value);
    print '7 Nilai tertinggi => '; 
    print implode(', ', array_slice($value, 0, 7));
    echo '<br/>';

    //Sort Terendah
    sort($value);
    print '7 Nilai Terendah => '; 
    print implode(', ', array_slice($value, 0, 7));
}

function countLowerCase($string)
{
    $count = 0;
    $explodes = str_split($string);
    foreach ($explodes as $explode) {
        if (ctype_lower($explode)) {
            $count++;
        }
    }
    echo 'Jumlah huruf kecil pada "'.$string.'" adalah => '. $count .' Huruf';
}

function stringGram($string)
{
    echo 'Kalimat Awal => '.$string;
    echo '<br/>';
    echo '<br/>';
    echo 'N-Gram pada soal.';
    echo '<br/>';
    echo 'Unigram => '. nGram2(1, $string);
    echo '<br/>';
    echo 'Bigram => '. nGram2(2, $string);
    echo '<br/>';
    echo 'Trigram => '. nGram2(3, $string);

    echo '<br/>';
    echo '<br/>';
    echo 'N-Gram yang biasanya digunakan.';
    echo '<br/>';
    echo 'Unigram => '. nGram(1, $string);
    echo '<br/>';
    echo 'Bigram => '. nGram(2, $string);
    echo '<br/>';
    echo 'Trigram => '. nGram(3, $string);
}

function nGram2($n = 1, $string)
{
    $ngrams = [];
    $split = explode(' ', $string);
    for ($i = 0; $i < count($split); $i++) {
        $pass_str[] = $split[$i];
        if (count($pass_str) == $n || count($split) == $i+1) {
            $ngrams[] = implode(' ', $pass_str);
            $pass_str = [];
        }
    }
    return implode(', ', $ngrams);
}

function nGram($n = 1, $string)
{
    $ngrams = [];
    $split = explode(' ', $string);
    for ($i = 0; $i < count($split) - $n + 1; $i++) {
        $ngrams[] = implode(' ', concatString($split, $i, $i + $n));
    }
    return implode(', ', $ngrams);
}

function concatString($words, $start, $end) {
    $string = [];
    for ($i = $start; $i < $end; $i++) {
        $string[] = $words[$i];
    }
    return $string;
}

function pre_print_r($var){
    echo "<pre>";
    print_r($var);
    echo "</pre>";
}

function enkripsi($string) {
    $encrypt = [];
    $string = strtoupper($string);
    $static = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $static_array = str_split($static);
    $split_string = str_split($string);
    for ($i=1; $i < count($split_string)+1; $i++) { 
        $index = $i;
        $find = array_search($split_string[$i-1], $static_array);
        if ($i > count($static_array)) {
            $index = $i % count($static_array);
        }
        if ($i % 2) {
            $check_index = $find+$index >= count($static_array) ? ($find+$index - count($static_array)) : $find+$index;
        } else {
            $check_index = $find-$index < 0  ? ($find-$index + count($static_array)) : $find-$index;
        }
        $encrypt[] = $static_array[$check_index];
    }
    echo 'Asal "'. $string .'" => "'.implode('', $encrypt).'"';
}

function tableOneTwo($length)
{
    $loop = 1;
    $next = [];
    $method = 'plus';
    $tables = [];
    for ($i=1; $i <= $length; $i++) { 
        if ($i % 3 == 0) {
            $next [] = $i;
            if ($loop % 2) {
                if ($method == 'plus') {
                    $next [] = ($loop*3) + 1;
                    $method = 'minus';
                } elseif ($method == 'minus') {
                    $next [] = ($loop*3) - 1;
                    $method = 'plus';
                }
            }
            $loop++;
        }
    }
    for ($j=1; $j <= $length; $j++) { 
        $color = 'black';
        if (in_array($j, $next)) {
            $color = 'white';
        }
        $tables[] = [
            $j, $color
        ];
    }
    $tables = array_chunk($tables, 8);
    return $tables;
}


function cari($arr = [], $string)
{
    $status = true;
    $rules = [];
    $explode = str_split($string);
    foreach ($explode as $s) {
        for ($i=0; $i < count($arr); $i++) { 
            if (in_array($s, $arr[$i])) {
                $x1 = null; // horizondatal right
                $y1 = null; // vertical up
                $x2 = null; // horizontal left
                $y2 = null; // vertical bottom
                $key_set = array_search($s, $arr[$i]);
                if (($key_set+1) <= 3) {
                    $x1 = [$i, $key_set+1];
                } 
                if (($i+1) <= 2) {
                    $y1 = [$i+1, $key_set];
                }
                if (($key_set-1) >= 0) {
                    $x2 = [$i, $key_set-1];
                }
                if (($i-1) >= 0) {
                    $y2 = [$i-1, $key_set];
                }
                $rules[$s] = [
                    'x1' => $x1,    
                    'y1' => $y1,    
                    'x2' => $x2,    
                    'y2' => $y2,    
                ];
            }
        }
    }
    for ($j=0; $j < count($explode); $j++) { 
        if ($status && $j+1 < count($explode)) {
            $x1_status = false;
            $y1_status = false;
            $x2_status = false;
            $y2_status = false;
            $char = $explode[$j];
            $match_char = $explode[$j+1];
            $rule = $rules[$char];
            if ($rule['x1']) {
                $x1_status = $arr[$rule['x1'][0]][$rule['x1'][1]] == $match_char;
            }
            if ($rule['y1']) {
                $y1_status = $arr[$rule['y1'][0]][$rule['y1'][1]] == $match_char;
            }
            if ($rule['x2']) {
                $x2_status = $arr[$rule['x2'][0]][$rule['x2'][1]] == $match_char;
            }
            if ($rule['y2']) {
                $y2_status = $arr[$rule['y2'][0]][$rule['y2'][1]] == $match_char;
            }
            $status = $x1_status || $y1_status || $x2_status || $y2_status;
        }
    }
    
    echo "cari(\$arr, '$string'); // ";
    echo $status ? 'true' : 'false';
    echo '<br/>';
}

$arr = [
    ['f', 'g', 'h', 'i'],
    ['j', 'k', 'p', 'q'],
    ['r', 's', 't', 'u'],
];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP DASAR</title>
</head>
<body>
    <?php nilaiUjian('72 65 73 78 75 74 90 81 87 65 55 69 72 78 79 91 100 40 67 77 86'); ?>
    <hr>
    <?php countLowerCase('TranSISI'); ?>
    <hr>
    <?php stringGram('Jakarta adalah ibukota negara Republik Indonesia'); ?>
    <hr>
    <table>
        <?php foreach (tableOneTwo(64) as $key => $value) { ?>
            <tr>
                <?php foreach ($value as $data) { ?>
                    <?php 
                        if ($data[1] == 'black') {
                    ?>
                        <td style="background-color: black;color:white;"><?= $data[0] ?></td>
                    <?php } else { ?> 
                        <td><?= $data[0] ?></td>
                    <?php } ?>
                <?php } ?>
            </tr>
        <?php } ?>    
    </table>
    <hr>
    <?php enkripsi('DFHKNQ'); ?>
    <hr>
    <?php cari($arr, 'fghi') ?>
    <?php cari($arr, 'fghp') ?>
    <?php cari($arr, 'fjrstp') ?>
    <?php cari($arr, 'fghq') ?>
    <?php cari($arr, 'fst') ?>
    <?php cari($arr, 'pqr') ?>
    <?php cari($arr, 'pghh') ?>
</body>
</html>