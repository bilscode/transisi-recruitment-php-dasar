## Instalasi
##### Clone repository
HTTP : `git clone https://gitlab.com/bilscode/transisi-recruitment-php-dasar.git`

##### Local Deployment
- Buka terminal baru dan jalankan `php -S localhost:8080`
- Buka browser dengan url `localhost:8080`

### Kontributor
* [bilscode](https://gitlab.com/bilscode) - **Billisany Akhyar** [billisanyakhyar24@gmail.com]
